/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.emergency;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.SigningInfo;
import android.content.pm.Signature;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.android.internal.util.ArrayUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Singleton utility class to validate a package signature against an allowlist.
 */
public class SignatureVerifier {
    private static final boolean DEBUG = false;

    private static final String TAG = "SignatureVerifier";
    private static final String SAFETYHUB_PACKAGE = "com.google.android.apps.safetyhub";

    /**
     * Byte array for faster matching of the release build's SHA-256 digest of SAFETY HUB:
     * 0xa0332928e61e86f1e683c29dc6e55f5181127b9f5ed8a7c4e84f9b8718f006e3
     *
     * To generate the above digest, run:
     * <code>apksigner verify --print-certs <apk></code>
     * and take the SHA-256 digest.
     *
     * apksigner can be found in the Android SDK's build-tools/<tools version>.
     */
    private static final byte[] RELEASE_DIGEST_SAFETYHUB = new byte[] {
            (byte) 0xa0, (byte) 0x33, (byte) 0x29, (byte) 0x28, (byte) 0xe6, (byte) 0x1e,
            (byte) 0x86, (byte) 0xf1, (byte) 0xe6, (byte) 0x83, (byte) 0xc2, (byte) 0x9d,
            (byte) 0xc6, (byte) 0xe5, (byte) 0x5f, (byte) 0x51, (byte) 0x81, (byte) 0x12,
            (byte) 0x7b, (byte) 0x9f, (byte) 0x5e, (byte) 0xd8, (byte) 0xa7, (byte) 0xc4,
            (byte) 0xe8, (byte) 0x4f, (byte) 0x9b, (byte) 0x87, (byte) 0x18, (byte) 0xf0,
            (byte) 0x06, (byte) 0xe3
    };

    /**
     * Checks if the package name is google signed, and on the allowlist.
     * The allowlist includes:
     * - SAFETY HUB
     *
     * @param name of the package.
     * @return true if package pass signature check.
     */
    public static boolean isCallerAllowlisted(Context context, String callingPackage) {
        try {
            PackageInfo packageInfo =
                    context.getPackageManager()
                            .getPackageInfo(callingPackage,
                                    PackageManager.GET_SIGNING_CERTIFICATES);

            String packageName = packageInfo.packageName;
            if (!isPackageAllowlisted(packageName)) {
                Log.e(TAG, "Package name: " + packageName + " is not allowlisted.");
                return false;
            }

            return isSignatureAllowlisted(packageInfo);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Could not find package name.", e);
            return false;
        }
    }

    // Enforce signature verification by ensuring that the package's signing certificate matches
    // one in the allowlist.
    private static boolean isSignatureAllowlisted(PackageInfo packageInfo) {
        if (packageInfo.signingInfo.getApkContentsSigners().length != 1) {
            Log.w(TAG, "Package has more than one signature.");
            return false;
        }

        byte[] signature = packageInfo.signingInfo.getApkContentsSigners()[0].toByteArray();
        boolean isValidSignature = isCertAllowlisted(signature);
        if (!isValidSignature && DEBUG) {
            Log.d(
                    TAG,
                    packageInfo.packageName
                            + " cert not in list. "
                            + " Cert:\n"
                            + Base64.encodeToString(signature, 0));
        }
        return isValidSignature;
    }

    private static boolean isCertAllowlisted(byte[] signature) {
        String algorithm = "SHA-256";
        byte[] certDigest = null;

        try {
            certDigest = MessageDigest.getInstance(algorithm).digest(signature);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Failed to obtain SHA-256 digest impl.", e);
            return false;
        }

        return Arrays.equals(certDigest, RELEASE_DIGEST_SAFETYHUB);
    }

    private static boolean isPackageAllowlisted(String packageName) {
        return TextUtils.equals(SAFETYHUB_PACKAGE, packageName);
    }
}
